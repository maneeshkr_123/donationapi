<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    //

    protected $fillable =[
        'name',
        'address',
        'pan_no',
        'aadhar_no',
        'contact_no',
        'ocupation',
    ];
}
